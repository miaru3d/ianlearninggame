﻿using UnityEngine;
using System.Collections;
using Juan;

public class JSoundButton : JButton {

	public AudioSource audioEmiter;
	public AudioClip soundToPlay;

	public override void OnPressed()
	{
		//audioEmiter.clip=soundToPlay;
		audioEmiter.PlayOneShot(soundToPlay);
	}
}
