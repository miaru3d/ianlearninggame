﻿using UnityEngine;
using System.Collections;
namespace Juan
{
	public class JAxis : MonoBehaviour {
		public bool active=true;
		public float factor=0.8f;
		public JButton UpButton;
		public JButton DownButton;
		public JButton LeftButton;
		public JButton RightButton;
		public float VerticalValue;
		public float HorizontalValue;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () 
		{
			if (DownButton.value & UpButton.value)
				VerticalValue=0f;
			else if (UpButton.value)
				VerticalValue=factor;
			else if (DownButton.value)
				VerticalValue=-factor;
			else
				VerticalValue=0f;
			////////
			if (LeftButton.value & RightButton.value)
				HorizontalValue=0f;
			else if (LeftButton.value)
				HorizontalValue=-factor;
			else if (RightButton.value)
				HorizontalValue=factor;
			else
				HorizontalValue=0f;
		}
	}
}
