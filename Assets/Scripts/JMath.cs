using System;
using System.Collections;
using System.Collections.Generic;

namespace Juan
{
	public class JMath
	{
		public JMath ()
		{

		}
		public static float clamp(float input,float max)
		{
			var s=Math.Sign(input);
			input=Math.Abs(input);
			if (input>max)
				input=max;
			return s*input;
		}
		public static float quarterSmooth(float input)
		{
			if (input==0f) return 0f;
			if (input ==1f) return 1f;
			var x=input+1;
			return -(x*x*x*x)+1;
		}
		public static float normalizeMinMax(float input, float minInput, float maxInput, float minOutput=0f,float maxOutput=1f)
		{
			var s= Math.Sign(input);
			input=Math.Abs(input);
			var r=((input)/(maxInput-minInput))+minOutput;
			if (r>maxOutput)
				r=maxOutput;
			return r*s;
		}
		public static float normalizeDegrees360(float n)
		{
			if (n < 360) 
			{
				return n;
			}
			else if (n>=360)
			{
				return normalizeDegrees360(n-360f);
			}
			else return 0f;
		}
		public static float normalizeDegrees180(float n)
		{
			if (n < 180) 
			{
				return n;
			}
			else if (n>=180)
			{
				return normalizeDegrees360(n-360f);
			}
			else return 0f;
		}
		public static float normalizeDegrees90(float n)
		{
			if (n < 90) 
			{
				return n;
			}
			else if (n>=90)
			{
				return normalizeDegrees360(n-360f);
			}
			else return 0f;
		}
	}
}

