﻿using UnityEngine;
using System.Collections;

namespace Juan
{
	[RequireComponent (typeof (RectTransform))]
	[RequireComponent (typeof (SpriteRenderer))]
	public class JButton : MonoBehaviour {

		public RectTransform r; 
		private Sprite defaultSprite;
		private Color defaultColor;
		public bool holdable=false;
		public Sprite activeSprite;
		public Color activeColor=Color.white;
		public bool value=false;
		public bool pressedInThisFrame=false;
		public float activeTime=0.2f;
		public float elapsedTime=0f;

		// Use this for initialization
		void Start () {
			r=GetComponent<RectTransform>();
			defaultSprite=this.GetComponent<SpriteRenderer>().sprite;
			defaultColor=this.GetComponent<SpriteRenderer>().color;
			this.GetComponent<SpriteRenderer>().sprite=defaultSprite;
			this.GetComponent<SpriteRenderer>().color=defaultColor;
		}
		public virtual void OnPressed()
			{

			}
		// Update is called once per frame
		void Update () {
			pressedInThisFrame=false;
			if (Input.touchCount>0 & value==false)
			{
				var touches= Input.touches;
				foreach (Touch t in touches)
				{
					if (t.phase==TouchPhase.Began)
					{
						if (RectTransformUtility.RectangleContainsScreenPoint(r,t.position,Camera.main))
						{
							value=true;
							Debug.Log("Button: "+this.name+" pressed");
							//StartCoroutine("reset");
							elapsedTime=0f;
							this.GetComponent<SpriteRenderer>().sprite=activeSprite;
							this.GetComponent<SpriteRenderer>().color=activeColor;
							this.pressedInThisFrame=true;
							this.OnPressed();
						}
					}
				}                                                    
			}
			if (Input.GetMouseButtonDown(0) & value==false)
			{
				if (RectTransformUtility.RectangleContainsScreenPoint(r,Input.mousePosition,Camera.main))
				{
					value=true;
					Debug.Log("Button: "+this.name+" pressed");
					//StartCoroutine("reset");
					elapsedTime=0f;
					this.GetComponent<SpriteRenderer>().sprite=activeSprite;
					this.GetComponent<SpriteRenderer>().color=activeColor;
					pressedInThisFrame=true;
					this.OnPressed();
				}
			}
			/////////////////////////////
			if (!holdable)
			{
				if (value)
				{
					elapsedTime+=Time.deltaTime;
					if (elapsedTime>activeTime)
					{
						Debug.Log("Button: "+this.name+" reseted");
						value =false;
						this.GetComponent<SpriteRenderer>().sprite=defaultSprite;
						this.GetComponent<SpriteRenderer>().color=defaultColor;
					}
				}
			}
			if (holdable)
			{
				if (value)
				{
					if(Input.GetMouseButtonUp(0))
					{
						value =false;
						this.GetComponent<SpriteRenderer>().sprite=defaultSprite;
						this.GetComponent<SpriteRenderer>().color=defaultColor;
					}
					//TODO: With touch
					var touches= Input.touches;
					foreach (Touch t in touches)
					{
						var tInButton=RectTransformUtility.RectangleContainsScreenPoint(r,t.position,Camera.main);
						if (tInButton & t.phase==TouchPhase.Ended)
						{
							value =false;
							this.GetComponent<SpriteRenderer>().sprite=defaultSprite;
							this.GetComponent<SpriteRenderer>().color=defaultColor;
						}
					}
				}
			}

		}
	}
}
