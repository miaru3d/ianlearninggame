﻿using UnityEngine;
using System.Collections.Generic;
using Juan;

public class JInputManager : MonoBehaviour {

	public string VerticalAxisName="Vertical";
	public string HorizontalAxisName="Horizontal";
	public string OllieButtonName="Ollie";
	public string FlipButtonName="Flip";
	public JAxis J_axis;
	public JButton FlipButton;
	public JButton OllieButton;
	public float cleanAfter=1f;
	public List<string> recent;
	public float counter=0f;
	// Use this for initialization
	

	void Start ()
	{
		recent.Add ("NONE");
		recent.Add ("NONE");
		recent.Add ("NONE");
		recent.Add ("NONE");
		recent.Add ("NONE");
		recent.Add ("NONE");
		recent.Add ("NONE");
	}

	public void addToRecent(string next)
	{
		if (next!=recent[2] | next=="NONE")
		{
			var templ=recent;
			recent[0]=templ[1];
			recent[1]=templ[2];
			recent[2]=templ[3];
			recent[3]=templ[4];
			recent[4]=templ[5];
			recent[5]=templ[6];
			recent[6]=next;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		counter+=Time.deltaTime;
		var v=Input.GetAxis(VerticalAxisName);
		var h=Input.GetAxis(HorizontalAxisName);
		var o=Input.GetAxis(OllieButtonName);
		var f=Input.GetAxis(FlipButtonName);
		if(v>0)
		{
			addToRecent("Up");
		}
		if (v<0)
		{
			addToRecent("Down");
		}
		if (h>0)
		{
			addToRecent("Right");
		}
		if(h<0)
		{
			addToRecent("Left");
		}
		if (o>0)
		{
			addToRecent("Ollie");
		}
		if(f>0)
		{
			addToRecent("Flip");
		}
		if (counter>cleanAfter)
		{
			counter=0f;
			addToRecent("NONE");
		}
	}
}
